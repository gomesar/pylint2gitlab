Custom tool for pylint [to show messages in Gitlab Merge Request][mr_code_quality].

## Installing:
Example:  
`pip3 install git+https://gitlab.com/gomesar/pylint2gitlab@dev`

## Using:
Example:  
`pylint -f pylint2gitlab.GLReporter ./**/*.py`


[mr_code_quality]: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
