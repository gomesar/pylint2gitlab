"""Gitlab codequality reporter"""
from __future__ import absolute_import, print_function
import hashlib
import html
import json
import os
import sys
from pylint.interfaces import IReporter
from pylint.reporters import BaseReporter


class GLReporter(BaseReporter):
    """Report messages and layouts in Gitlab format (json)."""
    __implements__ = IReporter
    name = "glreport"
    extension = "json"

    def __init__(self, output=sys.stdout):
        BaseReporter.__init__(self, output)
        self.messages = list()

    def handle_message(self, message):
        """Manage message of different type and in the context of path."""
        _string = '{}{}{}{}'.format(message.msg_id, message.path, message.line, message.column)
        _string = _string.encode('utf-8')
        fingerprint = hashlib.md5(_string).hexdigest()

        self.messages.append(
            {
                'type': 'issue',
                'check_name': message.symbol,
                'description': html.escape(message.msg or "", quote=False),
                'fingerprint': fingerprint,
                'categories': message.category,
                'location': {
                    'path': message.path,
                    'lines': {
                        'begin': {
                            'line': message.line,
                            'column': message.column,
                        },
                        'end': {
                            'line': message.line,
                            'column': message.column,
                        },
                    },
                },
                'severity': message.category,
            }
        )

    def display_messages(self, layout):
        """Launch layouts display"""
        print(json.dumps(self.messages, indent=2), file=self.out)

    def display_reports(self, layout):
        """Don't do nothing in this reporter."""
        pass

    def _display(self, layout):
        """Do nothing."""
        pass


def register(linter):
    """Register the reporter classes with the linter."""
    linter.register_reporter(GLReporter)
