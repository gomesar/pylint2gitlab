import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='pylint2gitlab',
    version='0.1',
    author="Alexandre R Gomes",
    author_email="gomes.bcc@gmail.com",
    description="Report from pylint to Gitlab",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gomesar/pylint2gitlab",
    packages=setuptools.find_packages(),
    install_requires=[
        'pylint',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
 )
